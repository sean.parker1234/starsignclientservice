
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ws.rs.core.MediaType;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.config.ClientConfig;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@WebServlet(name = "ClientServlet")
public class ClientServlet extends HttpServlet {
    static final String REST_URI = "http://localhost:9999/starsign";
    static final String DATE_URI = "date";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //gets user birth month and from HttpServletRequest object
        String dayString = request.getParameter("day");
        System.out.println(dayString);
        double day = Double.parseDouble(dayString);
        String month = request.getParameter("month");
        System.out.println(month);
        String[] responseElements = this.speakToWebServices(day,month);

        try (PrintWriter wr = response.getWriter()) {
            wr.println("<!DOCTYPE html><html");
            wr.println("<head>");
            wr.println("<meta charset=\"UTF=8\" />");
            wr.println("<title>Star-sign and Horoscope</title>");
            wr.println("</head>");
            wr.println("<body>");

            wr.println("<h1>Your star sign is:</h1>");
            wr.println("<h2>" + responseElements[0] + "</h2><br>");
            wr.println("<h1>Your horoscope of the day is:</h1>");
            wr.println("<h2>" + responseElements[2] + "</h2>");
            wr.println("<h1>And please our random joke for you today is:</h1>");
            wr.println("<h2>" + responseElements[1] + "</h2>");
            wr.println("</body>");
            wr.println("</html>");
        }

    }

    protected String[] speakToWebServices(double a, String b){
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(REST_URI);
        WebResource dateService = service.path(DATE_URI).path(a + "/" + b);

        ClientResponse responseCR = dateService.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        if (responseCR.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + responseCR.getStatus());
        }
        String responseString = responseCR.getEntity(String.class);
        JSONObject responseJSON = new JSONObject();
        try {
            responseJSON = new JSONObject(responseString);
        }catch(JSONException e){
            System.out.println("JSON exception");
            System.out.println(e);
        }
        //assign default values in case try statement fails
        String starSign = "empty";
        String joke = "no joke";
        String horoscope = "no horoscope";
        try{
            starSign = responseJSON.getString("starSign");
            joke = responseJSON.getString("joke");
            horoscope = responseJSON.getString("horoscope");

        } catch (JSONException e) {
            System.out.println("JSON get string method failed");
            e.printStackTrace();
        }
        String[] responseElements = {starSign, joke, horoscope};
        return responseElements;

    }



    protected String getOutputAsText(WebResource service){
        return service.accept(MediaType.TEXT_PLAIN).get(String.class);
    }
}
